(function($){

	var navClick = {
	
		init:function() {
		
			navClick.aboutClick();
			navClick.contactClick();
		
		},
	
		aboutClick:function(){
		
			$('.about').bind('click',function(){
			
				$('.header').addClass('header-active');
				$('.content .about').delay(700).fadeIn();
				$('.content .contact').fadeOut(800);
				
				return false;
			
			});
			
		},
		
		contactClick:function(){
		
			$('.contact').bind('click',function(){
			
				$('.header').addClass('header-active');
				$('.content .contact').delay(700).fadeIn();
				$('.content .about').fadeOut(800);
				
				return false;
			
			});
			
		}		
	
	};
	
	
	$(document).ready(function(){
	
		navClick.init();
	
	});
	
})(jQuery);
	